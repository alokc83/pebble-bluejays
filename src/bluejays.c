#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"


#define MY_UUID { 0xFC, 0x32, 0x82, 0xE0, 0x70, 0xC2, 0x43, 0x35, 0xB0, 0xBC, 0x9E, 0xC5, 0x43, 0xEF, 0x99, 0x82 }
PBL_APP_INFO(MY_UUID,
             "Blue Jays", "Scott Aitchison",
             1, 0, /* App version */
             DEFAULT_MENU_ICON,
             APP_INFO_WATCH_FACE);
             
Window window;
TextLayer text_time_layer;
GFont bjv_font_time;
TextLayer text_date_layer;
GFont bjv_font_date;
BmpContainer background_image;

void update_display(PblTm *tick_time) {
	static char time_text[] = "00 00";
	char *time_format;
	static char date_text[] = "mon 22";

	/* Blue Jay font doesn't come with a colon or dash */
	if (clock_is_24h_style()) {
		time_format = "%H %M";
	} 
	else {
		time_format = "%I %M";
	}

	string_format_time(time_text, sizeof(time_text), time_format, tick_time);
	if (!clock_is_24h_style() && (time_text[0] == '0')) {
		memmove(time_text, &time_text[1], sizeof(time_text) - 1);
	}

	text_layer_set_text(&text_time_layer, time_text);

	string_format_time(date_text, sizeof(date_text), "%a %d", tick_time);
	/* change first character to lower case */
	date_text[0] += 32;
	text_layer_set_text(&text_date_layer, date_text);
}

void handle_init(AppContextRef ctx) {
	(void)ctx;

	window_init(&window, "Window Name");
	window_stack_push(&window, false /* Animated */);
	window_set_background_color(&window, GColorBlack);
	resource_init_current_app(&APP_RESOURCES);

	/* Blue Jays image */
	bmp_init_container(RESOURCE_ID_IMAGE_BACKGROUND, &background_image);
	layer_set_frame(&background_image.layer.layer, GRect(22, 34, 100, 100));
	layer_add_child(&window.layer, &background_image.layer.layer);
	
	/* Time layer */
	text_layer_init(&text_time_layer, window.layer.frame);
	text_layer_set_text_color(&text_time_layer, GColorWhite);
	text_layer_set_background_color(&text_time_layer, GColorClear);
	layer_set_frame(&text_time_layer.layer, GRect(0, 132, 144, 168-132));
	text_layer_set_text_alignment(&text_time_layer, GTextAlignmentCenter);
	bjv_font_time = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_BJV_30));
	text_layer_set_font(&text_time_layer, bjv_font_time);
	layer_add_child(&window.layer, &text_time_layer.layer);

	/* Date layer */
	text_layer_init(&text_date_layer, window.layer.frame);
	text_layer_set_text_color(&text_date_layer, GColorWhite);
	text_layer_set_background_color(&text_date_layer, GColorClear);
	layer_set_frame(&text_date_layer.layer, GRect(0, 0, 144, 168-132));
	text_layer_set_text_alignment(&text_date_layer, GTextAlignmentCenter);
	bjv_font_date = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_BJV_26));
	text_layer_set_font(&text_date_layer, bjv_font_date);
	layer_add_child(&window.layer, &text_date_layer.layer);

	PblTm tick_time;
	get_time(&tick_time);
	update_display(&tick_time);
}


void handle_deinit(AppContextRef ctx) {
	(void)ctx;

	bmp_deinit_container(&background_image);
	fonts_unload_custom_font(bjv_font_time);
	fonts_unload_custom_font(bjv_font_date);
}

void handle_minute_tick(AppContextRef ctx, PebbleTickEvent *t) {
	(void)ctx;

	update_display(t->tick_time);
}

void pbl_main(void *params) {
	PebbleAppHandlers handlers = {
		.init_handler = &handle_init,
		.deinit_handler = &handle_deinit,
		.tick_info = {
			.tick_handler = &handle_minute_tick,
			.tick_units = MINUTE_UNIT
		}
	};
	app_event_loop(params, &handlers);
}
